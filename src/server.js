import express from 'express'
import { createServer } from 'http'
import { Server } from 'socket.io'
import path from "path";
import { fileURLToPath } from 'url';

const PORT = process.env.PORT || 3030;

const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);

const app = express();

const server = createServer(app)

const io = new Server(server)
 
app.use(express.static(path.join(__dirname, './public')));
 
app.get('/', function (req, res) {
  console.log(io)
  res.sendFile(path.join(__dirname, './index.html'));
});

const players = {};
var star = {
  x: Math.floor(Math.random() * 700) + 50,
  y: Math.floor(Math.random() * 500) + 50
};
var scores = {
  blue: 0,
  red: 0
};

io.on('connection', function (socket) {
  console.log('connect');
  console.log(socket.id);
  players[socket.id] = {
    rotation: 0,
    x: Math.floor(Math.random() * 700) + 50,
    y: Math.floor(Math.random() * 500) + 50,
    playerId: socket.id,
    team: (Math.floor(Math.random() * 2) == 0) ? 'red' : 'blue'
  };
  // отправляем объект players новому игроку
  socket.emit('currentPlayers', players);
  // отправляем объект star новому игроку
socket.emit('starLocation', star);
// отправляем текущий счет
socket.emit('scoreUpdate', scores);
  // обновляем всем другим игрокам информацию о новом игроке
  socket.broadcast.emit('newPlayer', players[socket.id]);
  socket.on('disconnect', function () {
    console.log('disconnect');
    // удаляем игрока из нашего объекта players 
delete players[socket.id];
// отправляем сообщение всем игрокам, чтобы удалить этого игрока
io.emit('discon', socket.id);
  });
  socket.on('playerMovement', function (movementData) {
    players[socket.id].x = movementData.x;
    players[socket.id].y = movementData.y;
    players[socket.id].rotation = movementData.rotation;
    // отправляем общее сообщение всем игрокам о перемещении игрока
    socket.broadcast.emit('playerMoved', players[socket.id]);
  });
  socket.on('starCollected', function () {
    if (players[socket.id].team === 'red') {
      scores.red += 10;
    } else {
      scores.blue += 10;
    }
    star.x = Math.floor(Math.random() * 700) + 50;
    star.y = Math.floor(Math.random() * 500) + 50;
    io.emit('starLocation', star);
    io.emit('scoreUpdate', scores);
  });
});
 
server.listen(PORT, function () {
  console.log(`Прослушиваем ${PORT}`);
});